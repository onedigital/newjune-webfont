# NewJune Webfont

[![npm package](https://img.shields.io/badge/npm-v4.1.2-green.svg)](https://npm.org) [![nodeJs](https://img.shields.io/badge/node-v7.7.1-green.svg)](https://nodejs.org) [![bower js](https://img.shields.io/badge/bower-1.8.0-green.svg)](http://bower.io/) ![licenca](https://img.shields.io/npm/l/express.svg)

----

### Primeiros passos
Antes de começar o projeto acesso o link de [primeiros passos](https://bitbucket.org/onedigital/docs/src/db3546173868cb6cd28c6b088fa1bc4bf1a511a6/FIRST_STEPS.md?at=master&fileviewer=file-view-default) siga as instruções para configurar todo o ambiente de desenvolvimento.

### Install
Adicionando as fontes via bower 

```sh
$ bower install --save https://bitbucket.org/onedigital/newjune-webfont.git
```
```sh
$ npm install --save bitbucket:onedigital/newjune-webfont
```

### Estruturas
A estrutura do segue o seguinte padrão:

- css  
    - newjune.css  
- sass  
    - newjune.scss  
- fonts  
    - bold (e variações)   
    - black (e variações)  
    - thin (e variações)  
    - light (e variações)  
    - medium (e variações)  
    - regular (e variações)  
    - heavy (e variações)  
    - semi-bold (e variações)  
    - ultra-light (e variações)  


### SASS
Caso queira colocar o SASS no lugar do CSS sobrescreva o bower.json

```json
"overrides": {
  "newjune-webfont": {
    "main": [
      "scss/newjune.scss",
      "fonts/**/*.{eot,svg,ttf,woff,woff2}"
    ]
  }
}
```
